package rationalsandpolynomials;

/**
 *
 * @author Alexis
 */
public class Monome {
    private int coeff, degre;

    /**
     * Crée un monome avec un coefficient et un degré;.
     * @param coeff coefficient du monome
     * @param degre degré; du monome
     */
    public Monome(int coeff, int degre)
    {
        this.coeff = coeff;
        this.degre = degre;
    }

    /**
     * Crée  un monome a partir d'un autre monome
     * @param m le monome a copier.
     */
    public Monome(Monome m)
    {
        this.coeff = m.coeff;
        this.degre = m.degre;
    }

    /**
     * Retourne le coefficient du monome
     *
     * @return coefficient
     */
    public int coeff()
    {
        return this.coeff;
    }

    /**
     * Retourne le degré du monome
     *
     * @return degré
     */
    public int degre()
    {
        return this.degre;
    }

    public boolean equals(Monome m)
    {
        if (this == m)
        {
            return true;
        }
        else if (m == null)
        {
            return false;
        }
        else
        {
            return (this.coeff == m.coeff && this.degre == m.degre);
        }
    }
	
    public String toString()
    {
        String affichage = "";
        if (this.coeff != 1)
            affichage += Integer.toString(this.coeff);
        if (this.degre != 0)
            affichage += "x^" + Integer.toString(this.degre);
        return affichage;
    }
}
