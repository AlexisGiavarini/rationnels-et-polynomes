package rationalsandpolynomials;

/**
 *
 * @author Alexis
 */
public class Rationnel {
    protected int numerateur;
    protected int denominateur;
    
    public Rationnel(int numerateur, int denominateur){
        if(denominateur != 0){
            this.numerateur = numerateur;
            this.denominateur = denominateur; 
        }else{
            throw new ArithmeticException("Denominator can't be 0");
        }       
    }
    
    /**
    * Returns the numerator
    * @return the numerator
    */
    public int numerateur(){
        return this.numerateur;
    }
    
    /**
    * Returns the denominator
    * @return the denominator
    */
    public int denominateur(){
        return this.denominateur;
    }
    
    /**
    * Returns the result of the division between two rationals
    * @param r the divisor
    * @return the rational number as result
    */
    public Rationnel divise(Rationnel r){
        Rationnel inverse = r.inverse();
        
        int produitNum = this.numerateur * inverse.numerateur();
        int produitDen = this.denominateur * inverse.denominateur();
        
        return new Rationnel(produitNum,produitDen);    
    }
    
    /**
    * Returns the result of the multiplication between two rationals
    * @param r the multiplier
    * @return the rational number as result
    */
    public Rationnel multiplie(Rationnel r){
        int produitNum = this.numerateur * r.numerateur();
        int produitDen = this.denominateur * r.denominateur();
        
        return new Rationnel(produitNum,produitDen);    
    }
    
    /**
    * Returns the result of the substraction between two rationals
    * @param r the number to substract
    * @return the rational number as result
    */
    public Rationnel moins(Rationnel r){
        int produitDen = this.denominateur * r.denominateur();
        int resNum = (this.numerateur * r.denominateur()) - (r.numerateur() * this.denominateur);
        
        return new Rationnel(resNum,produitDen);    
    }
    
    /**
    * Returns the result of the addition between two rationals
    * @param r the number to add
    * @return the rational number as result
    */
    public Rationnel plus(Rationnel r){
        int produitDen = this.denominateur * r.denominateur();
        int resNum = (this.numerateur * r.denominateur()) + (r.numerateur() * this.denominateur);
        
        return new Rationnel(resNum,produitDen);    
    }
    
    /**
    * Returns the real number
    * @return the real representation of the rational number
    */
    public double donneReel(){
        return this.numerateur / this.denominateur;
    }
    
    /**
    * Check if both the rational numbers are equal
    * @param r the rational number to compare
    * @return true if they are equal
    */
    public boolean equals (Rationnel r){
        if(this.donneReel() == r.donneReel()){
            return true;
        }else{
            return false;
        }    
    }
    
    /**
    * Returns the inverse of the rational number
    * @return the inverse
    */
    public Rationnel inverse(){     
        return new Rationnel(this.denominateur(), this.numerateur());    
    }
    
    /**
    * Returns the opposite of the rational number
    * @return the opposite number
    */
    public Rationnel oppose(){     
        return new Rationnel(this.numerateur(),-(this.denominateur()));    
    }
    
    public String toString(){
        return "Numérateur : "+this.numerateur+"\nDénominateur : "+this.denominateur;
    }
}
