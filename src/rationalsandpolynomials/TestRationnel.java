package rationalsandpolynomials;

/**
 *
 * @author Alexis
 */
public class TestRationnel {

    public static void main(String[] args) {
        Rationnel r1 = new Rationnel(2, 3);
        Rationnel r2 = new Rationnel(4, 5);

        // test pour l'addition de r1 et r2
        Rationnel resPlus = r1.plus(r2);
        Rationnel resPlusRef = new Rationnel(22, 15);
        if (!resPlus.equals(resPlusRef)) {
            System.out.println("Pb sur l'addition");
        }

        // test pour la soustraction de r1 et r2
        Rationnel resMoins = r1.moins(r2);
        Rationnel resMoinsRef = new Rationnel(-2, 15);
        if (!resMoins.equals(resMoinsRef)) {
            System.out.println("Pb sur la soustraction");
        }

        // test pour la multiplication de r1 et r2
        Rationnel resMultiplie = r1.multiplie(r2);
        Rationnel resMultiplieRef = new Rationnel(8, 15);
        if (!resMultiplie.equals(resMultiplieRef)) {
            System.out.println("Pb sur la multiplication");
        }

        // test pour la division de r1 et r2
        Rationnel resDivision = r1.divise(r2);
        Rationnel resDivisionRef = new Rationnel(10, 12);
        if (!resDivision.equals(resDivisionRef)) {
            System.out.println("Pb sur la division");
        }

        /* test de l'inverse qui consiste a appliquer 2 fois la methode inverse 
	a un rationnel et a verifier qu'au final on retrouve ce rationnel*/
        r1.inverse();
        r1.inverse();
        Rationnel resInverseRef = new Rationnel(2, 3);
        if (!r1.equals(resInverseRef)) {
            System.out.println("Pb sur l'inverse");
        }

        /* test de l'opposé qui consiste a appliquer 2 fois la methode oppose 
	a un rationnel et a verifier qu'au final on retrouve ce rationnel*/
        r1.oppose();
        r1.oppose();
        Rationnel resOpposeRef = new Rationnel(2, 3);
        if (!r1.equals(resInverseRef)) {
            System.out.println("Pb sur l'opposé");
        }

        // test pour le reel de r3
        Rationnel r3 = new Rationnel(10, 2);
        double resReel = r3.donneReel();
        double resReelRef = 5.0;
        if (resReel != resReelRef) {
            System.out.println("Pb sur le réel");
        }

        // test permettant de verifier qu'on ne peut pas 
        // créer un rationnel avec un dénominateur égal a zéro.
        
        //Rationnel r4 = new Rationnel(10, 0); 
    }

}
