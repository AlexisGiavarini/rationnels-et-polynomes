package rationalsandpolynomials;

/**
 *
 * @author Alexis
 */
/**
 * Représente un polynome de la forme : a_n x^n + a_n-1 x^n-1 + ... a_0 où a
 * sont les coefficients et n les degrés.
 *
 */
public class Polynome {

    private SequenceMonome seqMonome;

    /**
     * Crée un polynome vide
     */
    public Polynome() {
        this.seqMonome = new SequenceMonome();
    }

    /**
     * Crée un polynome à partir d'une tableau dont chaque
     * élément est lui-même un tableau de deux
     * éléments : le coefficient et le degré
     * @param listeCoeffDeg
     */
    public Polynome(int[][] listeCoeffDeg) {
        this.seqMonome = new SequenceMonome(listeCoeffDeg);
    }

    public String toString() {
        String affichage = "(";
        for (int i = 0; i < this.seqMonome.taille(); i++) {
            affichage += (i > 0 ? " + " : "") + this.seqMonome.donneMonome(i).toString();
        }
        return affichage + ")";
    }

    /**
     * Retourne le degré du polynome
     * @return degre
     */
    public int donneDegre() {
        return this.seqMonome.maxDegre();
    }

    /**
     * Retourne un polynome representant le resultat de l'addition du
     * polynome avec un autrepolynome p.
     * @param p polynome à additionner
     * @return resultat le polynome representant la somme de this et p.
     */
    public Polynome addition(Polynome p) {
        int tailleThis = this.seqMonome.taille();
        int tailleP = p.seqMonome.taille();
        Polynome resultat = new Polynome();
        int idxThis = 0, idxP = 0;
        // tant que la fin d'un des deux polynomes n'est pas atteinte
        while (idxThis < tailleThis && idxP < tailleP) {
            int degreThis = this.seqMonome.donneMonome(idxThis).degre();
            int coeffThis = this.seqMonome.donneMonome(idxThis).coeff();
            int degreP = p.seqMonome.donneMonome(idxP).degre();
            int coeffP = p.seqMonome.donneMonome(idxP).coeff();
            if (degreThis == degreP) {
                resultat.seqMonome.ajouterMonome(new Monome(coeffThis + coeffP, degreThis));
                idxThis++;
                idxP++;
            } else if (degreThis > degreP) {
                resultat.seqMonome.ajouterMonome(new Monome(this.seqMonome.donneMonome(idxThis)));
                idxThis++;
            } else // degreThis < degreP
            {
                resultat.seqMonome.ajouterMonome(new Monome(p.seqMonome.donneMonome(idxP)));
                idxP++;
            }
        }
        if (idxThis == tailleThis) {
            // recopie des monomes restants de this
            while (idxP < tailleP) {
                resultat.seqMonome.ajouterMonome(new Monome(p.seqMonome.donneMonome(idxP)));
                idxP++;
            }
        } else // recopie des monomes restant de p
        {
            while (idxThis < tailleThis) {
                resultat.seqMonome.ajouterMonome(new Monome(this.seqMonome.donneMonome(idxThis)));
                idxThis++;
            }
        }
        return resultat;
    }

    public Polynome derive() {
        Polynome resultat = new Polynome();
        
        for (int i = 0; i < this.seqMonome.taille(); i++) {
            if(this.seqMonome.donneMonome(i).degre() > 0){
                int coeffRes = this.seqMonome.donneMonome(i).coeff() * this.seqMonome.donneMonome(i).degre();
                int degreRes = this.seqMonome.donneMonome(i).degre()-1;
                
                resultat.seqMonome.ajouterMonome(new Monome(coeffRes,degreRes ));
            }
        }    
        return resultat;
    }

    public boolean equals(Polynome p) {
        if (this == p) {
            return true;
        } else if (p == null) {
            return false;
        } else if (this.donneDegre() != p.donneDegre()) {
            return false;
        } else if (this.seqMonome.taille() != p.seqMonome.taille()) {
            return false;
        } else {
            for (int i = 0; i < this.seqMonome.taille(); i++) {
                if (!this.seqMonome.donneMonome(i).equals(p.seqMonome.donneMonome(i))) {
                    return false;
                }
            }
            return true;
        }
    }
}
