package rationalsandpolynomials;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */

/**
 * Représente une suite ordonnée de monômes organisée par degré décroissant. 
 * Cette classe est utilisée par le classe Polynome pour s'abstraire de la 
 * structure de donnée utilisée pour stocker les monômes 
 */
public class SequenceMonome {
    //private int [][] listeCoeffDeg;
    private ArrayList<Monome> listeCoeffDeg = new ArrayList<Monome>();

    /**
     * Crée une séquence vide de monômes
     */
    public SequenceMonome() {
    }
    
    /**
     * Crée une séquence de monômes à partir d'un tableau dont chaque élément
     * est lui-même un tableau constitué de deux éléments : le coefficient et
     * le degré
     */
    public SequenceMonome(int[][] listeCoeffDeg) {
        int [][] listeCoeffDegTmp = listeCoeffDeg ;
        int tmpCoeff;
        int tmpDegre;
        int cpt = 0; //Nombre de monomes dont le coeff vaut 0;
        
        //Tri décroissant des monomes
        for (int i = 0; i < listeCoeffDegTmp.length; i++) {
            if(listeCoeffDegTmp[i][0] == 0){
                cpt++;
            }
            for (int j = 0; j < listeCoeffDegTmp.length; j++) {
                if(listeCoeffDegTmp[i][1] > listeCoeffDegTmp[j][1]){
                    tmpCoeff = listeCoeffDegTmp[i][0];
                    tmpDegre = listeCoeffDegTmp[i][1];
                    
                    listeCoeffDegTmp[i][0] = listeCoeffDegTmp[j][0];
                    listeCoeffDegTmp[i][1] = listeCoeffDegTmp[j][1];
                    
                    listeCoeffDegTmp[j][0] = tmpCoeff;
                    listeCoeffDegTmp[j][1] = tmpDegre;   
                }        
            }
        }
        
        //Supression des monomes dont le coeff est 0
//        this.listeCoeffDeg = new int [listeCoeffDegTmp.length-cpt][];
//        int j = 0;
//        for (int i = 0; i < listeCoeffDegTmp.length; i++) {
//            if(listeCoeffDegTmp[i][0] != 0){
//                this.listeCoeffDeg[j] = listeCoeffDegTmp[i];
//                j++;
//            }
//        }

            for (int i = 0; i < listeCoeffDegTmp.length; i++) {
                if(listeCoeffDegTmp[i][0] != 0){
                    this.listeCoeffDeg.add(new Monome(listeCoeffDegTmp[i][0],listeCoeffDegTmp[i][1]));
                }
            }
        }
    
    /**
     * Ajoute un monôme à la séquence
     * @param mono le monome à ajouter
     */
    public void ajouterMonome(Monome mono){
//        int majListeCoeffDeg [][] = new int[][]{{mono.coeff(),mono.degre()}};
//
//        if(this.listeCoeffDeg != null){
//            int AjoutListeCoeffDeg [][] = new int[this.taille()+1][];
//            
//            for (int i = 0; i < AjoutListeCoeffDeg.length; i++) {
//                if(i < this.taille()){
//                    AjoutListeCoeffDeg[i] = this.listeCoeffDeg[i];
//                }else{
//                    AjoutListeCoeffDeg [i] = majListeCoeffDeg[0];
//                }       
//            }
//            this.listeCoeffDeg = AjoutListeCoeffDeg;
//        }else{
//            this.listeCoeffDeg = majListeCoeffDeg;
//        }   
        if(this.listeCoeffDeg != null){
            this.listeCoeffDeg.add(mono);
        }else{
            this.listeCoeffDeg = new ArrayList<Monome>();
            this.listeCoeffDeg.add(mono);
            
        }      
    }
    
    /**
     * Retourne le monôme présent à la position donnée par l'index
     * @param index la position où chercher
     * @return le monome
     */
    public Monome donneMonome(int index){
        //return new Monome(this.listeCoeffDeg[index][0], this.listeCoeffDeg[index][1]);
        return new Monome(this.listeCoeffDeg.get(index));
    }
    
    /**
     * Retourne le plus grand degré des monômes de la séquence
     * @return le plus grand degré
     */
    public int maxDegre(){
//        int max = this.listeCoeffDeg[0][1];
//        
//        for (int i = 0; i < this.listeCoeffDeg.length; i++) {
//            if(this.listeCoeffDeg[i][1] > max){
//                max = this.listeCoeffDeg[i][1];
//            }
//        }
//        return max;

        int max = this.listeCoeffDeg.get(0).degre();
        
        for (int i = 0; i < this.listeCoeffDeg.size(); i++) {
            if(this.listeCoeffDeg.get(i).degre() > max){
                max = this.listeCoeffDeg.get(i).degre();
            }
        }
        return max;
    }
    
    /**
     * Retourne le nombre de monômes de la séquence
     * @return la taille de la sequence
     */
    public int taille(){
        //return this.listeCoeffDeg.length;
        return this.listeCoeffDeg.size();
    }
}
